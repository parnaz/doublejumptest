// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "SphereTrace.generated.h"

/**
 * 
 */
UCLASS()
class DOUBLEJUMPTEST_API USphereTrace : public USphereComponent
{
	GENERATED_BODY()
	
public:
	USphereTrace(const FObjectInitializer& ObjectInitializer);
	float SphereRadius;
	USphereComponent* SphereTracer;
};
