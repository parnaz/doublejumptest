// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"
#include "ClimbingPawnMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class DOUBLEJUMPTEST_API UClimbingPawnMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
// public:

// 	UPROPERTY(EditAnywhere)
// 	USphereComponent* SphereTracer;

// 	UPROPERTY(EditAnywhere)
// 	float SphereRadius;
};
