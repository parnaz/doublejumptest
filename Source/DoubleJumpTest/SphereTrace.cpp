// Fill out your copyright notice in the Description page of Project Settings.


#include "SphereTrace.h"

USphereTrace::USphereTrace(const FObjectInitializer& ObjectInitializer)
{
    SphereTracer = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereTrcacer"));
	SphereTracer->InitSphereRadius(15.0f);
}